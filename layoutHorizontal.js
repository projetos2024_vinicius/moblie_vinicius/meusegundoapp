import React from "react";
import { StyleSheet, Text, View, ScrollView, Image } from "react-native";
import { StatusBar } from "expo-status-bar";

export default function LayoutHorizontal() {
  return (
    <View style={styles.container}>
      <View style={styles.box1}></View>
      <View style={styles.box2}></View>
      <View style={styles.box3}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
  },
  box1: { 
    width: 50,
     height: 50, 
     backgroundColor: "red" 
    },
  box2: {
    width: 50,
     height: 50, 
     backgroundColor: "pink" 
  },
  box3: {
    width: 50,
     height: 50, 
     backgroundColor: "green" 
  },
});
