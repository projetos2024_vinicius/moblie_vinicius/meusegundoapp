import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Entypo, Feather, Foundation, MaterialIcons, AntDesign, SimpleLineIcons, FontAwesome, Ionicons, Fontisto } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native'; // Importe useNavigation
import { StatusBar } from 'expo-status-bar';

export default function LayoutDeTelaEstrutura() {
  const navigation = useNavigation(); // Inicialize useNavigation

  const handlePress = () => {
    navigation.navigate('LayoutHorizontal'); // Navegue para o componente 'LayoutHorizontal' ao pressionar
  };
  const handlePress2 = () => {
    navigation.navigate('LayoutGrade'); // Navegue para o componente 'LayoutHorizontal' ao pressionar
  };
  const handlePress3 = () => {
    navigation.navigate('Components'); // Navegue para o componente 'LayoutHorizontal' ao pressionar
  };

  return (
    <View style={styles.container}>

      <ScrollView>
        <View style={styles.header}>
          <Text style={{ color: "white", }}>Instagram.Clone</Text>
          <View style={{ flexDirection: "row" }}>
            <Feather name="heart" size={24} color="white" style={{right:20,}}  />
            <AntDesign name="message1" size={24} color="white" />
          </View>
        </View>
        <ScrollView horizontal pagingEnabled style={styles.storyContainer}>
          <View style={styles.story}>
            <MaterialIcons name="account-circle" size={60} color="white" />
          </View>
          <View style={styles.story}>
            <MaterialIcons name="account-circle" size={60} color="white" />
          </View>
          <View style={styles.story}>
            <MaterialIcons name="account-circle" size={60} color="white" />
          </View>
          <View style={styles.story}>
            <MaterialIcons name="account-circle" size={60} color="white" />
          </View>
          <View style={styles.story}>
            <MaterialIcons name="account-circle" size={60} color="white" />
          </View>
          <View style={styles.story}>
            <MaterialIcons name="account-circle" size={60} color="white" />
          </View>
          {/* Add more stories as needed */}
        </ScrollView>

        <View style={styles.perfil}>
          <MaterialIcons name="account-circle" size={40} color="white" />
          <Text style={{ color: "white", top: 10, marginLeft: 5 }}>
            Perfil 1
          </Text>
          <SimpleLineIcons
            name="options-vertical"
            size={18}
            color="white"
            style={{ left: 245, top: 10 }}
          />
        </View>
        <View style={styles.content}>
          <Image
            source={require("./images/carlos.jpg")}
            style={{ width: "100%", height: 300 }}
          />
        </View>
        <View style={{ flexDirection: "row" }}>
          <Feather name="heart" size={30} color="white" style={{ left: 10 }} />
          <FontAwesome
            name="comment-o"
            size={30}
            color="white"
            style={{ left: 30 }}
          />
          <Ionicons
            name="paper-plane-outline"
            size={30}
            color="white"
            style={{ left: 50, top: 5 }}
          />
          <Fontisto
            name="favorite"
            size={28}
            color="white"
            style={{ left: 240, top: 3 }}
          />
        </View>
        <View style={styles.perfil}>
          <MaterialIcons name="account-circle" size={40} color="white" />
          <Text style={{ color: "white", top: 10, marginLeft: 5 }}>
            Perfil 2
          </Text>
          <SimpleLineIcons
            name="options-vertical"
            size={18}
            color="white"
            style={{ left: 245, top: 10 }}
          />
        </View>
        <View style={styles.content2}>
          <Image
            source={require("./images/vitao.jpg")}
            style={{ width: "100%", height: 300 }}
          />
        </View>
        <View style={{ flexDirection: "row" }}>
          <Feather name="heart" size={30} color="white" style={{ left: 10 }} />
          <FontAwesome
            name="comment-o"
            size={30}
            color="white"
            style={{ left: 30 }}
          />
          <Ionicons
            name="paper-plane-outline"
            size={30}
            color="white"
            style={{ left: 50, top: 5 }}
          />
          <Fontisto
            name="favorite"
            size={28}
            color="white"
            style={{ left: 240, top: 3 }}
          />
        </View>
        <View style={styles.perfil}>
          <MaterialIcons name="account-circle" size={40} color="white" />
          <Text style={{ color: "white", top: 10, marginLeft: 5 }}>
            Perfil 3
          </Text>
          <SimpleLineIcons
            name="options-vertical"
            size={18}
            color="white"
            style={{ left: 245, top: 10 }}
          />
        </View>
        <View style={styles.content}>
          <Image
            source={require("./images/igor.png")}
            style={{ width: "100%", height: 300 }}
          />
        </View>
        <View style={{ flexDirection: "row" }}>
          <Feather name="heart" size={30} color="white" style={{ left: 10 }} />
          <FontAwesome
            name="comment-o"
            size={30}
            color="white"
            style={{ left: 30 }}
          />
          <Ionicons
            name="paper-plane-outline"
            size={30}
            color="white"
            style={{ left: 50, top: 5 }}
          />
          <Fontisto
            name="favorite"
            size={28}
            color="white"
            style={{ left: 240, top: 3 }}
          />
        </View>
        <View style={styles.perfil}>
          <MaterialIcons name="account-circle" size={40} color="white" />
          <Text style={{ color: "white", top: 10, marginLeft: 5 }}>
            Perfil 4
          </Text>
          <SimpleLineIcons
            name="options-vertical"
            size={18}
            color="white"
            style={{ left: 245, top: 10 }}
          />
        </View>
        <View style={styles.content}>
          <Image
            source={require("./images/finzetto.jpg")}
            style={{ width: "40%", height: 300 }}
          />
        </View>
        <View style={{ flexDirection: "row" }}>
          <Feather name="heart" size={30} color="white" style={{ left: 10 }} />
          <FontAwesome
            name="comment-o"
            size={30}
            color="white"
            style={{ left: 30 }}
          />
          <Ionicons
            name="paper-plane-outline"
            size={30}
            color="white"
            style={{ left: 50, top: 5 }}
          />
          <Fontisto
            name="favorite"
            size={28}
            color="white"
            style={{ left: 240, top: 3 }}
          />
        </View>
      </ScrollView>

      <View style={styles.footer}>
        <Entypo name="home" size={40} color="white" />
        <Feather name="search" size={40} color="white" />
        <TouchableOpacity onPress={handlePress3}> 
        <Entypo name="squared-plus" size={40} color="white" />
        </TouchableOpacity>
        <TouchableOpacity onPress={handlePress2}> 
        <Foundation name="play-video" size={40} color="white" />
        </TouchableOpacity>
        <TouchableOpacity onPress={handlePress}> 
          <Image
            style={{ borderRadius: 120 }}
            source={require('./images/EU.jpg')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: "black",
  },
  storyContainer: {
    flexDirection: "row",
    height: 80,
  },
  story: {
    alignItems: "center",
    justifyContent: "center",
    width: 70,
    height: 70,
    backgroundColor: "lightblue",
    marginHorizontal: 5,
    borderRadius: 60,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 50,
    backgroundColor: "black",
  },
  content: {
    alignItems: "center",
    flex: 1,
    backgroundColor: "black",
  },
  content2: {
    alignItems: "center",
    flex: 1,
    backgroundColor: "black",
  },

  perfil: {
    flexDirection: "row",
    alignItems: "start",
    top: 0,
  },

  footer: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: 50,
    backgroundColor: "black",
  },
});
