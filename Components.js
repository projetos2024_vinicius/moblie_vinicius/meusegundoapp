import React from "react";
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from "react-native";
import { useState } from "react";

export default function LayoutGrade() {
  const [text, setText] = useState("");
  const [numLetras, setNumLetras] = useState('0')
  function click(){
    setNumLetras(text.length)
  }
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Com quantos pontos o corinthians cai? {text}</Text>
      <TextInput
      placeholder="Digite Algo"
      style={styles.input}
      value={text}
      onChangeText={(textInput) => setText (textInput) }
      />
      <Button
      style={styles.text}
      title="Click"
      onPress={() => {click()}}
      />
      {numLetras > 0 ? 
      <Text>{numLetras}</Text>
       : 
       <Text></Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
},

  text:{
    fontSize:24,
    fontWeight:'bold',
  },
  input:{
    borderWidth:1,
    width:"80%",
    borderColor:'gray',
    padding:10,
    marginVertical:10,

  }
});
