import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Layout from './layoutDeTelaEstrutura'; 
import LayoutHorizontal from './layoutHorizontal'; 
import LayoutGrade from './layoutGrade'; 
import Components from './Components'
const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Layout} options={{ headerShown: false }} />
        <Stack.Screen name="LayoutHorizontal" component={LayoutHorizontal} options={{ title: 'Layout Horizontal' }} />
        <Stack.Screen name="LayoutGrade" component={LayoutGrade} options={{ title: 'Layout Grade' }} />
        <Stack.Screen name="Components" component={Components} options={{ title: 'Componentes' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
});
